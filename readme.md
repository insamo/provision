# Provision for web-server

## Content

- Nginx, php7, php7-fpm, hhvm, composer, node, redis, blackfire, memcache, beanstalkd

## OS

- ubuntu-server 14.04 production, development
- ubuntu-server 16.04 production, development

## Install

- Clone Repository
- Add file flag executable (sudo chmod +x ./install_%env%-ubuntu-%version%.sh)
- Run script (sudo ./install_%env%-ubuntu-%version%.sh)
