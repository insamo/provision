# Change Log

## [Unreleased][unreleased]

## [0.0.6] - 2017-08-25
### Added
- Added original provision file
- Added new aliases
### Changed
- Changed version php(5.7,7.0,7.1)
- Changed timezone define
- Changed scripts location now in /vagrant/scripts

## [0.0.5] - 2016-12-07
### Added
- Added munin-node installation
- Added .bash_aliases file

### Changed
- Changed nginx host config file(added browser caching)

## [0.0.4] - 2016-11-04
### Added
- Added installation instruction to readme.md
### Changed
- Changed ubuntu-server-16.04 production timezone
- Changed libsqlite3 to stable release
### Removed
- Removed remote access to mysql
- Removed some trash

## [0.0.3] - 2016-10-27
### Added
- Added ubuntu-server-16.04

## [0.0.2] - 2015-10-02
### Added
- Added production, development env

## [0.0.1] - 2015-10-01
### Added
- Added ubuntu-server-14.04

# Change Log FAQ

[FAQ LINK](http://keepachangelog.com/)

## [Added] for new features.
## [Changed] for changes in existing functionality.
## [Deprecated] for once-stable features removed in upcoming releases.
## [Removed] for deprecated features removed in this release.
## [Fixed] for any bug fixes.
## [Security] to invite users to upgrade in case of vulnerabilities.
